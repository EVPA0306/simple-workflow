package gof.bdp;

import gof.bdp.state.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProcessBuilder {

    private String processId;
    private List<Task> tasks = new ArrayList<>();

    private ProcessBuilder(String processId, List<Task> tasks) {
        this.processId = processId;
        this.tasks.addAll(tasks);
    }

    public String getProcessId() {
        return processId;
    }

    public Task getTask(int index) {
        return tasks.get(index);
    }

    public int getTaskCount() {
        return tasks.size();
    }


    public static class Builder implements gof.bdp.Builder<ProcessBuilder> {

        private final String processId;
        private final List<Task> tasks;

        private Builder() {
            processId = UUID.randomUUID().toString();
            tasks = new ArrayList<>();
        }

        public static ProcessBuilder.Builder builder() {
            return new ProcessBuilder.Builder();
        }

        public ProcessBuilder.Builder add(Task task) {
            tasks.add(task);
            return this;
        }

        @Override
        public ProcessBuilder build() {
            return new ProcessBuilder(processId, tasks);
        }
    }
}
