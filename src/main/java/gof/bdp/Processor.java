package gof.bdp;

import gof.bdp.state.InProgress;
import gof.bdp.state.ProcessContext;
import gof.bdp.state.Task;

public class Processor {

    private Task currentTask;
    private ProcessContext context;

    public Processor(Task currentTask, ProcessContext context) {
        super();
        this.currentTask = currentTask;
        this.context = context;

        if (currentTask == null) {
            this.currentTask = InProgress.getInstance();
        }
    }

    public void setCurrentTask(Task currentTask) {
        this.currentTask = currentTask;
    }

    public void setContext(ProcessContext context) {
        this.context = context;
    }

    public ProcessContext getContext() {
        return context;
    }

    public void run() {
        currentTask.execute(this);
    }
}
