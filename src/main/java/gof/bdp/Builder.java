package gof.bdp;

public interface Builder<T> {
    T build();
}
