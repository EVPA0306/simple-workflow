package gof.bdp;

import gof.bdp.state.CorrectionsRequired;
import gof.bdp.state.Expired;
import gof.bdp.state.InProgress;
import gof.bdp.state.PendingComplianceApproval;
import gof.bdp.state.ProcessContext;

public class MainWorkFlow {

    public static void main(String[] args) {

        ProcessContext context = new ProcessContext();
        context.put("userName", "Eugene");
        context.put("accessId","12345");

        ProcessBuilder processBuilder = ProcessBuilder.Builder
                .builder()
                .add(InProgress.getInstance())
                .add(PendingComplianceApproval.getInstance())
                .add(CorrectionsRequired.getInstance())
                .add(Expired.getInstance())
                .build();


        Processor processor = new Processor(processBuilder.getTask(0), context);

        for (int i = 0; i < processBuilder.getTaskCount(); i++) {
            processor.setCurrentTask(processBuilder.getTask(i));
            processor.run();
        }


        System.out.println("Restart task");
        int currentTask = 1;
        processor.setCurrentTask(processBuilder.getTask(currentTask));
        for (int i = currentTask; i < processBuilder.getTaskCount(); i++) {
            processor.run();
        }
    }
}
