package gof.bdp.state;

import lombok.Getter;

import java.util.UUID;

@Getter
public abstract class AbstractTask implements Task {

    public Status status = Status.CREATED;

    private String id = UUID.randomUUID().toString();

}
