package gof.bdp.state;

import gof.bdp.Processor;

public class CorrectionsRequired extends AbstractTask {

    private static Task INSTANCE = new CorrectionsRequired();

    public static Task getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Processor processor) {
        System.out.println("Task ID: " + getId());

        System.out.println("Bio is in Corrections Required\n");
        //processor.setCurrentTask(PendingComplianceApproval.getInstance());
        status = Status.COMPLETED;
    }
}
