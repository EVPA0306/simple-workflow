package gof.bdp.state;

import gof.bdp.Processor;

public class ComplianceApproved extends AbstractTask {

    private static Task instance = new ComplianceApproved();

    private ComplianceApproved() {}

    public static Task getInstance() {
        return instance;
    }

    @Override
    public void execute(Processor processor) {
        System.out.println("Bio is in Compliance Approved");
        //processor.setCurrentTask(Expired.getInstance());
        status = Status.COMPLETED;
    }
}
