package gof.bdp.state;

import gof.bdp.Processor;

public class InProgress extends AbstractTask {

    private static final Task INSTANCE = new InProgress();

    private InProgress() {}

    public static Task getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Processor processor) {

        System.out.println("Task ID: " + getId());

        System.out.println("Bio is In Progress status");
        ProcessContext context = processor.getContext();
        System.out.println("Task context: " + context.get("userName") +" " + context.get("accessId") + "\n");

        //processor.setCurrentTask(PendingComplianceApproval.getInstance());
        status = Status.COMPLETED;
    }
}
