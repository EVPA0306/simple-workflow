package gof.bdp.state;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ProcessContext {
    private Map<String, Object> wfContext = new ConcurrentHashMap<>();

    public void put(String key, Object value) {
        wfContext.put(key, value);
    }

    public Object get(String key) {
        return wfContext.get(key);
    }
}
