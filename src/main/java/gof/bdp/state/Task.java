package gof.bdp.state;

import gof.bdp.Processor;

public interface Task {
    void execute(Processor processor);
    String getId();
}
