package gof.bdp.state;

import gof.bdp.Processor;

public class Expired extends AbstractTask {

    private static Task instance =  new Expired();

    private Expired() {}

    public static Task getInstance() {
        return instance;
    }

    @Override
    public void execute(Processor processor) {
        status = Status.COMPLETED;
    }
}
