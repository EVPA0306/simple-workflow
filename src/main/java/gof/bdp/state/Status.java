package gof.bdp.state;

public enum Status {
    /**
     * The task is created
     */
    CREATED,
    /**
     * The task is running
     */
    RUNNING,
    /**
     * The task has failed
     */
    FAILED,
    /**
     * The task has been completed successfully
     */
    COMPLETED
}
