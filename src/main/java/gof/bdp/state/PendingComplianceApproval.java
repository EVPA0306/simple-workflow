package gof.bdp.state;

import gof.bdp.Processor;

public class PendingComplianceApproval extends AbstractTask {

    private static PendingComplianceApproval INSTANCE = new PendingComplianceApproval();

    private PendingComplianceApproval() {}

    public static Task getInstance() {
        return INSTANCE;
    }
    @Override
    public void execute(Processor processor) {
        System.out.println("Task ID: " + getId());

        System.out.println("Bio is in Pending Compliance Approval status\n");
        processor.setCurrentTask(ComplianceApproved.getInstance());
        //or
        //processor.setCurrentTask(CorrectionsRequired.getInstance());

        status = Status.COMPLETED;
    }
}
